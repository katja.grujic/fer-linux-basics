#!/bin/bash

# Stvorite direktorij LAB1 i premjestite se u njega.
mkdir LAB1
cd LAB1

# Stvorite novi direktorij source i u njemu prazan file empty.

mkdir source
touch source/empty

# U direktorij source kopirajte sadrˇzaj direktorija /boot i direktorija /etc.
sudo cp -avr /boot/. /etc/. source/

# Ispiˇsite zauze´ce direktorija source koriste´ci SI prefikse (potencije broja 10) mjernih jedinica.
sudo du -sh --si source

# U direktoriju LAB1 stvorite simboliˇcku poveznicu target na direktorij source.
sudo ln -s source target

# Premjestite se u direktorij target bez dereferenciranja poveznice i ispiˇsite adresu trenutnog direktorija.
#		Pokaˇzite da ispis adrese trenutnog direktorija daje LAB1/target.
cd target
echo $PWD

# Vratite se u direktorij LAB1 i premjestite se u direktorij target koriste´ci dereferenciranje poveznice.
#		Pokaˇzite da ispis adrese trenutnog direktorija daje LAB1/source.
cd ..
cd -P target
echo $PWD

# Koriste´ci poveznicu target odredite veliˇcinu direktorija source.
cd ..
stat -Lc %s target

# Koriste´ci naredbu touch stvorite praznu datoteku source/novi i postavite joj vrijeme izmjene (mtime)
#		tako da bude isto kao i datoteci source/empty. Koristite jednu naredbu.
sudo touch -m source/novi -r source/empty


# Izbriˇsite sve stvorene direktorije i datoteke u direktoriju LAB1 koriste´ci jednu naredbu.
sudo rm -rf *

# Izbriˇsite prazan direktorij LAB1 koriste´ci naredbu za brisanje praznog direktorija.
cd ..
rmdir LAB1/
