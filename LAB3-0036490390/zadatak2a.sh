#! /bin/bash

echo "Pokretna traka PID: $$"

kaps=0
kompr=0
sumec=0

trap "((kaps++))" 1
trap "((kompr++))" 2
trap "((sumec++))" 3

while true ; do
	printf "Kapsule: %s\n" $kaps
	printf "Komprimirane: %s\n" $kompr
	printf "Sumece: %s\n" $sumec
	echo "-------------------------"
	sleep 1
done