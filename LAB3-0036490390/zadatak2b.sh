#! /bin/bash

#getting id of zadatak2a.sh
scriptPID=$(pgrep -f zadatak2a.sh)

count=0
echo "Saljem na PID $scriptPID."

#on interrupt signal (ctrl+c)
trap finish INT

finish() {
	printf "\nUkupno: $count tableta.\n"
	kill -9 $scriptPID
	kill -9 $$
}

while true; do
	sleep 1
	sigval=$((1 + RANDOM %3))
	case $sigval in
		1 )
			vrsta="kapsulu.";;
		2 )
			vrsta="komprimiranu tabletu.";;
		3 )
			vrsta="sumecu tabletu.";;
	esac
	kill -$sigval $scriptPID
	echo "Saljem $vrsta"
	echo $sigval
done