#!/bin/bash

for user in `studenti.txt`
do
	echo "$user"
	adduser -m -k /etc/skel "$user" --shell /bin/bash -g studenti 
	echo "student1" | passwd --stdin "$user"
	chown "$user" -r /home/studenti/$user
	chmod 700 /home/studenti/$user
done