
sudo adduser --no-create-home  --disabled-password --shell /bin/sh stjepan 

visudo
stjepan ALL=NOPASSWD: /usr/sbin/adduser, /usr/sbin/usermod, /usr/sbin/deluser, /usr/sbin/addgroup, /usr/sbin/groupmod, /usr/sbin/delgroup, /bin/chmod, /bin/chown

sudo addgroup studenti

sudo mkdir /home/studenti
sudo chmod a+r a-w /home/studenti

sudo mkdir /home/studenti/studenti-shared
sudo chown --recursive :studenti /home/studenti/studenti-shared
sudo chmod g=rwx o-rwx /home/studenti/studenti-shared

sudo mkdir -p /etc/skel/{Documents,Download}
sudo ln -s /home/studenti/studenti-shared /etc/skel/Shared
