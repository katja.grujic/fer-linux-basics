
#get job pid
scriptPID=$(jobs -p)

#view nice number
ps -o pid,nice $scriptPID

#changing nice number of running script will alter its priority
renice -n 15 -p $scriptPID


#CTRL+Z suspends foregound process to background by sending SIGTSTP signal, suspended process is not terminated
#CTRL+C terminates foreground process by sending SIGINT (interrupt) signal
#if process is in background, it needs to be killed with SIGKILL signal
#instead of keyboard shortcuts, signals can be sent with command 'kill [-<signal code>] <PID>'
kill -9 $scriptPID
#after exiting terminal, all processes in that terminal are terminated with signal SIGHUP (hang up)
#signal hang up is a signal sent to a process when its controlling terminal is closed
#if we want to have process that will stay alive after his parent terminal is closed we need to call it with command 'nohup'
#nohup runs a command imumune to hangups 'nohup COMMAND [ARG]'
nohup sh lab03-3.sh &
