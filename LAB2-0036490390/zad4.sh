#!/bin/bash

find / -name "*.py" | xargs sed -n 's/^\(\s*def\) \(\w*\)\(.*\)/\2/p'

read

find / -name "*.c" | xargs sed -n '/^#/p'

read

grep -nr "include" ./test
