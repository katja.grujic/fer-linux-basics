#!/bin/bash
dir="./test"

files=( $(find ${dir} -type f) )

echo "PRVI DIO"
for file in ${files[@]}
do
	regex="^c[[:alpha:]]*[[:digit:]]{2}\.[[:alpha:]]+"
	filename=$(basename "$file")

	if [[ "${filename}" =~ ${regex} ]]
	then
		echo "${file}"
	fi
done

read

echo "DRUGI DIO"
for file in ${files[@]}
do
	regex2="^[^f-p]+$"
	regex3="[0-9]+"
	filename=$(basename "$file")

	if [[ "${filename}" =~ ${regex2} && "${filename}" =~ ${regex3} ]]
	then
		echo "${file}"
	fi
done
