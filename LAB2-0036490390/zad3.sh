#!/bin/bash

filename="Top10.txt"

touch ${filename}

cat << EOF > ${filename}
Linux Mint 17.2
Ubuntu 15.10
Debian GNU/Linux 8.2
Mageia 5
Fedora 23
openSUSE Leap 42.1
Arch Linux
CentOS 7.2-1511
PCLinuxOS 2014.12
Slackware Linux 14.1
FreeBSD
EOF

printf "Tekst prije obrade: \n\n"
cat ${filename}

sed -ri 's/[^0-9]*$//g' ${filename}
sed -ie 's/^\(.*\) \([[:digit:]\.-]*\)$/\2 \1/g' ${filename}
sed -ie 's/\(.*\)/\L\1/' ${filename}
sed -ri 'y/aeiou/AEIOU/' ${filename}

sort -V ${filename} -o ${filename}

printf "\n\nTekst nakon obrade: \n"
cat ${filename}
