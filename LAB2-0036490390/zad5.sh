#!/bin/bash

dir="./test/"

files=( $(find ${dir} -type f) )

for file in ${files[@]}
do
	filename=$(basename "$file")
	rename 's/(PNG)-([0-9]{2})([0-9]{2})([0-9]{4})/$2_$3_$4.\L$1/' ${dir}${filename}
done
