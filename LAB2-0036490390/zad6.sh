#!/bin/bash
files="./downloads/*"

for file in ${files}
do
	underscore_regex="([[:alpha:]]+[[:digit:]]*)_"
	ime_predmeta="([[:alpha:]]+[[:digit:]]*)"
	razonoda="./downloads/razonoda"
	filename=$(basename "$file")

	if [[ "${filename}" =~ ${underscore_regex} ]]; then
		if [[ "${filename}" =~ ${ime_predmeta} ]]; then
			predmet=(${BASH_REMATCH})
			folder="./downloads/"$predmet
			if [ ! -d "$folder" ]; then
				mkdir $folder
			fi
			mv "$file" $folder
		fi
	else
		if [ ! -d "$razonoda" ]; then
			mkdir $razonoda
		fi
		mv "$file" $razonoda
	fi

done

files="./downloads/razonoda/*"

for file in ${files}
do
	knjige_regex="((.*)\.(pdf)|(epub))"
	slike_regex="((.*)\.(jpg)|(jpeg))"
	muzika_regex="((.*)\.mp3)"
	knjige="./downloads/razonoda/knjige"
	slike="./downloads/razonoda/slike"
	muzika="./downloads/razonoda/muzika"
	filename=$(basename "$file")

	if [[ "${filename}" =~ ${knjige_regex} ]]; then
			if [ ! -d "$knjige" ]; then
				mkdir $knjige
			fi
			mv "$file" $knjige
	fi

	if [[ "${filename}" =~ ${slike_regex} ]]; then
			if [ ! -d "$slike" ]; then
				mkdir $slike
			fi
			mv "$file" $slike
	fi

	if [[ "${filename}" =~ ${muzika_regex} ]]; then
			if [ ! -d "$muzika" ]; then
				mkdir $muzika
			fi
			mv "$file" $muzika
	fi

done


